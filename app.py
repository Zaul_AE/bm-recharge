
import os
from PIL import Image

import dash
from dash import Dash, html, dcc
from dash import callback, Output, Input, State
import dash_bootstrap_components as dbc


#%% App config
PATH = os.path.dirname(os.path.abspath(__file__))
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(PATH, "analytics.json")

#%% App body
app = Dash(
    __name__,
    use_pages=True,
    external_stylesheets=[
        dbc.themes.FLATLY,
        dbc.icons.FONT_AWESOME
    ],
    meta_tags=[
        {"title": "BM-Recharge"},
        {"charset": "utf-8"},
        {"name": "viewport", "content": "width=device-width, initial-scale=1"},
        {"name": "robots", "content": "index,follow"},
        {
            "ref": "https://fonts.googleapis.com/css2?family=Ubuntu:wght@500&display=swap",
            "rel": "stylesheet",
        }
    ]
)

app.index_string = """
<html>
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MTJGLX27');</script>
        <!-- End Google Tag Manager -->
        {%metas%}
        <title>{%title%}</title>
        {%favicon%}
        {%css%}
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MTJGLX27"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            {%renderer%}
        </footer>
    </body>
</html>
"""

server = app.server

lang_icon = html.I(className="fa fa-language", style=dict(display="inline-block"))

navbar = dbc.NavbarSimple(
    children=[
        dbc.NavItem(dbc.NavLink("Inicio", href="/"), id="menu1"),
        dbc.NavItem(dbc.NavLink("App", href="/App", id="link-menu2"), id="menu2"),
        dbc.NavItem(dbc.NavLink("Documentacion", href="/Docs"), id="menu3"),
        dbc.NavItem(dbc.NavLink("Acerca", href="/About"), id="menu4"),
        dbc.DropdownMenu(
            [
                dbc.DropdownMenuItem("English", href="/?lang=en", id="lang-option")
            ],
            label=lang_icon,
            nav=True,
            id="menu5"
        )
    ],
    brand="BM-Recharge",
    brand_href="#",
    color="primary",
    dark=True,
)
layout = html.Div(
    children=[
        navbar,
        dcc.Store(id="memory", storage_type="local"),
        html.Div([dash.page_container], id="page-layout-app")
    ]
)

app.layout = layout


#%% Callbacks

@callback(
    [Output("menu1", "children"),
     Output("menu2", "children"),
     Output("menu3", "children"),
     Output("menu4", "children"),
     Output("lang-option", "children"),
     Output("lang-option", "href"),
     Output("memory", "data"),
    ],
    [Input("lang-option", "n_clicks")],
    [State("lang-option", "children"),
     State("memory", "data")],
    prevent_initial_call=True,
)
def change_language(n_clicks, lang, data):
    data = data or dict(lang="es")
    if lang == "Español":
        # Pages properties
        dash.page_registry["pages.home"]["path"] = "/"
        dash.page_registry["pages.home"]["title"] = "Inicio"
        dash.page_registry["pages.map"]["title"] = "Aplicacion"
        dash.page_registry["pages.docs"]["title"] = "Documentacion"
        dash.page_registry["pages.about"]["title"] = "Acerca"
        # Navigation properties
        menu_item1 = dbc.NavLink("Inicio", href="/")
        menu_item2 = dbc.NavLink("App", href="/App")
        menu_item3 = dbc.NavLink("Documentacion", href="/Docs")
        menu_item4 = dbc.NavLink("Acerca", href="/About")
        lang = "English"
        href = "/?lang=en"
        data["lang"] = "es"
    else:
        # Pages properties
        dash.page_registry["pages.home"]["path"] = "/"
        dash.page_registry["pages.home"]["title"] = "Home"
        dash.page_registry["pages.map"]["title"] = "Application"
        dash.page_registry["pages.docs"]["title"] = "Documentation"
        dash.page_registry["pages.about"]["title"] = "About"
        # Navigation properties
        menu_item1 = dbc.NavLink("Home", href="/?lang=en")
        menu_item2 = dbc.NavLink("App", href="/App?lang=en")
        menu_item3 = dbc.NavLink("Documentation", href="/Docs?lang=en")
        menu_item4 = dbc.NavLink("About", href="/About?lang=en")
        lang = "Español"
        href = "/"
        data["lang"] = "en"
    return menu_item1, menu_item2, menu_item3, menu_item4, lang, href, data


#%% Run app
if __name__ == '__main__':
    app.run_server(debug=False)
    
