

import os
import json
import toml
import numpy as np
import pandas as pd
from PIL import Image
from calendar import monthrange

import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

import dash
from dash import Dash, html, dcc
from dash import callback, Output, Input, State
import dash_bootstrap_components as dbc


#%% Configuration data

PATH = os.path.dirname(os.path.abspath(__file__))

MIN_COLOR = (8, 48, 107)
MID_COLOR = (15, 168, 227)
MAX_COLOR = (111, 198, 107)


#%% Load data

def get_dataset_name(dataset):
    if dataset == "m1":
        return "Estaciones"
    elif dataset == "m2":
        return "CHIRPS"
    elif dataset == "m3":
        return "CHIRPS-C"
    elif dataset == "m4":
        return "CHIRPS-Daymet"


def load_labels(lang):
    if lang == "es":
        labels_file = os.path.join(PATH, "static", "labels", "plots_labels_es.toml")
    else:
        labels_file = os.path.join(PATH, "static", "labels", "plots_labels_en.toml")
    with open(labels_file) as fid:
        labels = toml.load(fid)
    return labels


def load_layers():
    """Load layers"""

    grid = json.load(open(os.path.join(PATH, "layers", "Grid.geojson"), "r", encoding="utf-8"))
    aquifers = json.load(open(os.path.join(PATH, "layers", "Aquifers.geojson"), "r", encoding="utf-8"))
    zones = json.load(open(os.path.join(PATH, "layers", "Zones.geojson"), "r", encoding="utf-8"))
    layers = {
        "grid": grid,
        "aquifers": aquifers,
        "zones": zones
    }
    return layers


def load_data_layers():
    """Load data for layers"""
    
    grid = pd.read_csv(os.path.join(PATH, "layers", "Grid.csv")).set_index("ID", drop=False)
    aquifers = pd.read_csv(os.path.join(PATH, "layers", "Aquifers.csv")).set_index("ID", drop=False)
    zones = pd.read_csv(os.path.join(PATH, "layers", "Zones.csv")).set_index("ID", drop=False)
    data = {
        "grid": grid,
        "aquifers": aquifers,
        "zones": zones
    }
    return data


def load_map_series(year, dataset, dtype, varname):
    """Read map timeseries"""
    dataset = get_dataset_name(dataset)
    filename = os.path.join(PATH, "data", f"{dtype}_{varname}_Annual_{dataset}")
    df = pd.read_feather(filename).set_index("Dates")
    serie = df.loc[year, :]
    serie.index = [int(x) for x in serie.index]
    return serie


def get_area(layer_data, dtype, ids):
    if ids is None:
        if dtype == "grid":
            area = layer_data[dtype].loc[:, "Area (km2)"].sum()
        else:
            area = layer_data[dtype].set_index("Name").loc[:, "Area (km2)"]
    else:
        idx = [int(x) for x in ids]
        if dtype == "grid":
            area = layer_data[dtype].loc[idx, :].set_index("Name")["Area (km2)"]
        else:
            area = layer_data[dtype].loc[idx, :].set_index("Name")["Area (km2)"]
        if len(ids) == 1:
            area = area.values[0]
    
    return area


def read_timeseries(dataset, dtype, variables, ptype, ids, area, units, lang="es"):
    """Read timeseries at different timescales"""
    dataset = get_dataset_name(dataset)
    if ids is not None:
        ids = ["Dates"] + ids
    series = {}
    for varname in variables:
        if ptype in ("Monthly", "Seasonal"):
            if dtype == "grid" and ids is None:
                filename = os.path.join(PATH, "data", f"basin_{varname}_Monthly_{dataset}")
            else:
                filename = os.path.join(PATH, "data", f"{dtype}_{varname}_Monthly_{dataset}")
            try:
                df = pd.read_feather(filename, columns=ids).set_index("Dates")
            except:
                df = pd.read_feather(filename).set_index("Dates")
            
            if dtype == "grid" and ids is None:
                if lang == "es":
                    df.columns = ["Cuenca"]
                else:
                    df.columns = ["Basin"]
            else:
                map_labels = load_data_layers()
                cols = map_labels[dtype].loc[:, ["ID", "Name"]].set_index("ID").squeeze()
                idx = [int(x) for x in df.columns]
                df.columns = cols.loc[idx].to_list()

            if ptype == "Monthly":
                series[varname] = df
            elif ptype == "Seasonal":
                series[varname] = df.groupby(df.index.month).mean()
            
        elif ptype in ("Annual", "Anomaly"):
            if dtype == "grid" and ids is None:
                filename = os.path.join(PATH, "data", f"basin_{varname}_Annual_{dataset}")
            else:
                filename = os.path.join(PATH, "data", f"{dtype}_{varname}_Annual_{dataset}")
            try:
                df = pd.read_feather(filename, columns=ids).set_index("Dates")
            except:
                df = pd.read_feather(filename).set_index("Dates")

            if dtype == "grid" and ids is None:
                if lang == "es":
                    df.columns = ["Cuenca"]
                else:
                    df.columns = ["Basin"]
            else:
                map_labels = load_data_layers()
                cols = map_labels[dtype].loc[:, ["ID", "Name"]].set_index("ID").squeeze()
                idx = [int(x) for x in df.columns]
                df.columns = cols.loc[idx].to_list()

            if ptype == "Annual":
                series[varname] = df
            elif ptype == "Anomaly":
                series[varname] = df - df.mean()
    
        if dtype == "grid" and ids is None:
            series[varname].columns = ["Cuenca"]
    
    for key in series.keys():
        series[key] = convert_units(series[key], area, units, ptype)
    return series


def days_in_month(dates):
    return np.array([monthrange(d.year, d.month)[1] for d in dates])


def convert_units(data, area, units="mm", ptype="Monthly"):
    if "mm" in units:
        return data
    new_data = data.copy()
    if "s" in units:
        if ptype == "Monthly":
            dates = data.index
            days = days_in_month(dates)
            if isinstance(data, pd.DataFrame):
                new_data.values[:] = data.values / days[:, None]
            else:
                new_data.values[:] = data.values / days
        elif ptype == "Seasonal":
            days = np.array([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
            if isinstance(data, pd.DataFrame):
                new_data.values[:] = data.values / days[:, None]
            else:
                new_data.values = data.values / days
        elif ptype in ("Annual", "Anomaly"):
            new_data.values[:] = data.values / 365.
        new_data /= 86400.
    if "lp" in units:
        new_data = (new_data * area) * 1e6
    elif "m3" in units:
        new_data = (new_data * area) * 1e3
    elif "ft3" in units:
        new_data = (new_data * area) * 1e3 * 35.3147
    return new_data


def download_layer(layer, layer_data, year, dataset, dtype, varname, lang="es"):
    labels = load_labels(lang)
    serie = load_map_series(year, dataset, dtype, varname)
    attributes = layer_data[dtype].copy()
    # convert units
    factor = 365 * 86400
    serie_lps = (serie * attributes["Area (km2)"] * 1e6 / factor).round(5)
    serie_cms = (serie * attributes["Area (km2)"] * 1e3 / factor).round(5)
    serie_cfts = (serie * attributes["Area (km2)"] * 1e3 * 35.3147 / factor).round(5)
    label_mm = f"{varname}_mm"
    label_lps = f"{varname}_lps"
    label_cms = f"{varname}_m3-s"
    label_cfts = f"{varname}_ft3-s"

    new_layer = layer[dtype].copy()
    for feat in new_layer["features"]:
        idx = feat["properties"]["ID"]
        for col in attributes.columns:
            feat["properties"]["Area (km2)"] = float(attributes.loc[idx, "Area (km2)"])
            feat["properties"][label_mm] = float(serie.loc[idx])
            feat["properties"][label_lps] = float(serie_lps.loc[idx])
            feat["properties"][label_cms] = float(serie_cms.loc[idx])
            feat["properties"][label_cfts] = float(serie_cfts.loc[idx])
    return new_layer


#%% Plot functions

def get_n_colors(n):
    """Interpolate colors for time series"""
    
    if n == 0:
        colors = []
    elif n == 1:
        colors = [f"rgb{str(MIN_COLOR)}"]
    elif n == 2:
        colors = [f"rgb{str(MIN_COLOR)}", f"rgb{str(MID_COLOR)}"]
    elif n == 3:
        colors = [
            f"rgb{str(MIN_COLOR)}",
            f"rgb{str(MID_COLOR)}",
            f"rgb{str(MAX_COLOR)}"
            ]
    else:
        n1 = int(np.round(n / 2))
        n2 = n - n1
        color_tuples1 = px.colors.n_colors(MIN_COLOR, MID_COLOR, n1)
        color_tuples2 = px.colors.n_colors(MID_COLOR, MAX_COLOR, n2 + 1)
        colors = [f"rgb{str(color)}" for color in color_tuples1]
        colors.extend([f"rgb{str(color)}" for color in color_tuples2][1:])
    return colors


def create_map(layer, layer_data, year, dataset, dtype, varname, cmap, vmin, vmax, lang="es"):
    """Create the main map"""
    vmin, vmax = min(vmin, vmax), max(vmin, vmax)
    labels = load_labels(lang)
    serie = load_map_series(year, dataset, dtype, varname)
    attributes = layer_data[dtype].copy()
    # convert units
    factor = 365 * 86400
    serie_lps = (serie * attributes["Area (km2)"] * 1e6 / factor).round(5)
    serie_cms = (serie * attributes["Area (km2)"] * 1e3 / factor).round(5)
    serie_cfts = (serie * attributes["Area (km2)"] * 1e3 * 35.3147 / factor).round(5)
    label_mm = f"{varname} (mm)"
    label_lps = f"{varname} (lps)"
    label_cms = f"{varname} (m3/s)"
    label_cfts = f"{varname} (ft3/s)"
    attributes[label_mm] = serie
    attributes[label_lps] = serie_lps
    attributes[label_cms] = serie_cms
    attributes[label_cfts] = serie_cfts

    attributes.columns = (labels["attributes"][dtype]
        + [label_mm, label_lps, label_cms, label_cfts])
    
    fig = px.choropleth_mapbox(
        attributes,
        geojson=layer[dtype],
        color=label_mm,
        locations="ID",
        featureidkey="properties.ID",
        hover_data=attributes.columns,
        range_color=(vmin, vmax),
        opacity=0.6,
        center={"lat": 19.58775, "lon": -98.87349},
        color_continuous_scale=cmap,
        mapbox_style="carto-positron",
        zoom=8.
    )
    
    fig.update_layout(
        margin={"r":0,"t":0,"l":0,"b":0},
        clickmode="event+select",
    )

    return fig


def plot_series(series, units="mm", lang="es"):

    labels = load_labels(lang)
    n_rows = len(series)
    fig = make_subplots(rows=n_rows, cols=1, shared_xaxes=True)
        
    for row, varname in enumerate(series.keys()):
        plot_data = series[varname]
        if row == 0:
            colors = get_n_colors(plot_data.shape[1])
        for i, col in enumerate(plot_data.columns):
            if row == 0:
                show_legend = True
            else:
                show_legend = False
            data = go.Scatter(
                x=plot_data.index,
                y=plot_data[col].values,
                mode="lines",
                name=col,
                showlegend=show_legend,
                line=dict(color=colors[i])
                )
            fig.add_trace(data, row=row+1, col=1)
            # update layout
            ylabel = f"{labels['plots'][varname]} ({units})"
            if row == 0:
                fig["layout"]["yaxis"].update(title=ylabel)
            else:
                fig["layout"][f"yaxis{row+1}"].update(title=ylabel)

    fig.update_layout(
        autosize=True,
        height=n_rows*200,
        margin=dict(
            l=10,
            r=10,
            b=10,
            t=10,
            pad=3
        ),
        legend={
            "orientation":"h",
            "yanchor": "bottom",
            "y": 1.02,
            "xanchor": "right",
            "x": 1
        }
    )
    #fig.update_layout(hovermode="x")

    return fig

