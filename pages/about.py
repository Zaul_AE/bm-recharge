

import os
import toml
import dash
from dash import html, dcc, callback, Input, Output
import dash_bootstrap_components as dbc


#%% Config
PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


#%% Page Layout
dash.register_page(__name__, path="/About", title="Acerca")

def layout(lang="es"):
    if lang not in ("es", "en"):
        lang = "es"
    with open(os.path.join(PATH, "static", "labels", f"about_labels_{lang}.toml"), "r", encoding="utf-8") as fid:
        labels = toml.load(fid)

    project_section = html.Div(
        dbc.Col(
            [
                html.H2(labels["project"]["header1"]),
                dcc.Markdown(labels["project"]["text1"]),
            ],
            id="project-section"
        ),
        id="project-section-div"
    )
    card1 = dbc.Card(
        dbc.CardBody(
            [
                html.H4(labels["team"]["user1"]),
                dcc.Markdown(labels["team"]["text1"]),
                dbc.CardLink("Twitter", href="https://twitter.com/zaul_arciniega"),
                dbc.CardLink("LinkedIn", href="https://www.linkedin.com/in/saularciniegaesparza/"),
                dbc.CardLink("ResearchGate", href="https://www.researchgate.net/profile/Saul-Arciniega-Esparza"),
            ]
        ),
        class_name="card-member"
    )
    card2 = dbc.Card(
        dbc.CardBody(
            [
                html.H4(labels["team"]["user2"]),
                dcc.Markdown(labels["team"]["text2"],
                ),
                dbc.CardLink("Twitter", href="https://twitter.com/hydrogeologymx"),
                dbc.CardLink("ResearchGate", href="https://www.researchgate.net/profile/Antonio-Hernandez-Espriu"),
            ]
        ),
        class_name="card-member"
    )
    card3 = dbc.Card(
        dbc.CardBody(
            [
                html.H4(labels["team"]["user3"]),
                dcc.Markdown(labels["team"]["text3"]),
                dbc.CardLink("LinkedIn", href="https://www.linkedin.com/in/gabriel-salinas-calleros-40a4954a/"),
            ]
        ),
        class_name="card-member"
    )
    card4 = dbc.Card(
        dbc.CardBody(
            [
                html.H4(labels["team"]["user4"]),
                dcc.Markdown(labels["team"]["text4"]),
                dbc.CardLink("LinkedIn", href="https://www.linkedin.com/in/sergio-gonzalez-ortigoza-47a97024a/"),
            ]
        ),
        class_name="card-member"
    )
    cards = html.Div(
        [
            html.H2(labels["team"]["header1"]),
            dbc.CardGroup(
                [
                card1,
                card2,
                card3,
                card4
            ])
        ],
        id="group-section"
    )
    institution_section = html.Div(
        html.Div(
            [
                html.Div(
                    [
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/UNAM_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.unam.mx/"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/EAS_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/puei/Espaguasub.html"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src=f"../static/logos/HydrogeologyGroup_{lang}.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/hydrogeology/"
                            ),
                            class_name="logo-col"
                        ),
                    ],
                    id="logo-container"
                )
            ],
            id="institution-section-body"
        )
        ,
        id="institution-section"
    )
    page_layout = html.Div(
        [
            project_section,
            cards,
            institution_section
        ],
        id="about-body"
    )
    return page_layout

