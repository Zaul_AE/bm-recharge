

import os
import dash
from dash import html, dcc, callback, Input, Output
import dash_bootstrap_components as dbc


#%% Config
PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


#%% Page Layout
dash.register_page(__name__, path="/Docs", title="Documentacion")

def layout(lang="es"):
    if lang not in ("es", "en"):
        lang = "es"
    with open(os.path.join(PATH, "static", "labels", f"docs-{lang}.txt"), "r", encoding="utf-8") as fid:
        docs_text = fid.read()

    intro_section = html.Div(
        [
            dbc.Col(
                dcc.Markdown(docs_text),
            )
        ],
        id="docs-intro-div"
    )
    institution_section = html.Div(
        html.Div(
            [
                html.Div(
                    [
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/UNAM_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.unam.mx/"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/EAS_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/puei/Espaguasub.html"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src=f"../static/logos/HydrogeologyGroup_{lang}.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/hydrogeology/"
                            ),
                            class_name="logo-col"
                        ),
                    ],
                    id="logo-container"
                )
            ],
            id="institution-section-body"
        )
        ,
        id="institution-section"
    )
    page_layout = html.Div(
        [
            html.Br(),
            html.Br(),
            intro_section,
            html.Br(),
            html.Br(),
            institution_section
        ],
        id="docs-body"
    )
    return page_layout

