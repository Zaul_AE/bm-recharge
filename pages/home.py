
import os
import toml
import dash
from dash import html, dcc, callback, Input, Output, State
import dash_bootstrap_components as dbc


#%% Config
PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

#%% Page Layout
dash.register_page(__name__, path='/', title="Inicio")

def layout(lang="es", **other_strings):
    if lang not in ("es", "en"):
        lang = "es"
    with open(os.path.join(PATH, "static", "labels", f"home_labels_{lang}.toml"), "r", encoding="utf-8") as fid:
        labels = toml.load(fid)
    # INTRO - SECTION
    intro_section = html.Div(
        [
            html.H1("BM-Recharge"),
            html.H2(labels["intro"]["label1"]),
            html.Br(),
            html.H3(labels["intro"]["label2"]),
            html.Br(),
            html.A(labels["intro"]["label3"], href="https://www.ingenieria.unam.mx/hydrogeology/"),
            html.A(labels["intro"]["label4"], href="https://www.unam.mx/"),
        ],
        id="intro-section"
    )
    # DESCRIPTION - SECTION
    description_section = dbc.Col(
        [
            html.H2(labels["description"]["header1"]),
            dcc.Markdown(labels["description"]["text1"]),
            dbc.Row(
                html.Div(
                    dbc.Button(labels["description"]["button3"], class_name="button", href=f"/App?lang={lang}"),
                    className="description-buttons"
                ),
            ),
            dcc.Markdown(labels["description"]["text2"]),
            html.Div(
                [
                    dbc.Button(labels["description"]["button1"], class_name="button", href=f"/Docs?lang={lang}"),
                    dbc.Button(labels["description"]["button2"], class_name="button", href=labels["description"]["href_button2"])
                ],
                className="description-buttons"
            )
        ],
        id="description-section"
    )
    # PRODUCT - SECTION
    card1 = dbc.Card(
        [
            dbc.Row(
                dbc.CardImg(src="../static/icons/wave.png", top=True, class_name="card-img"),
                class_name="card-img-container"
            ),
            dbc.CardBody(
                dcc.Markdown(labels["product"]["card1"])
            ),
        ],
        class_name="product-card",
    )
    card2 = dbc.Card(
        [
            dbc.Row(
                dbc.CardImg(src="../static/icons/chart.png", top=True, class_name="card-img"),
                class_name="card-img-container"
            ),
            dbc.CardBody(
                dcc.Markdown(labels["product"]["card2"])
            ),
        ],
        class_name="product-card",
    )
    card3 = dbc.Card(
        [
            dbc.Row(
                dbc.CardImg(src="../static/icons/satellite.png", top=True, class_name="card-img"),
                class_name="card-img-container"
            ),
            dbc.CardBody(
                dcc.Markdown(labels["product"]["card3"])
            ),
        ],
        class_name="product-card",
    )
    product_section = html.Div(
        [
            html.Div(
                [
                    html.H2(labels["product"]["hedaer1"]),
                    dbc.Row(
                        [
                            dbc.Col(card1),
                            dbc.Col(card2),
                            dbc.Col(card3),
                        ]
                    )
                ],
                id="product-section-body"
            ),
        ],
        id="product-section"
    )
    # INSTITUTION - SECTION
    institution_section = html.Div(
        html.Div(
            [
                html.H2(labels["acknowledgment"]["header1"]),
                dcc.Markdown(labels["acknowledgment"]["text1"]),
                html.Div(
                    [
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/UNAM_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.unam.mx/"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src="../static/logos/EAS_w.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/puei/Espaguasub.html"
                            ),
                            class_name="logo-col"
                        ),
                        dbc.Col(
                            html.A(
                                html.Img(
                                    src=f"../static/logos/HydrogeologyGroup_{lang}.png",
                                    className="logo-img"
                                ),
                                href="https://www.ingenieria.unam.mx/hydrogeology/"
                            ),
                            class_name="logo-col"
                        ),
                    ],
                    id="logo-container"
                )
            ],
            id="institution-section-body"
        )
        ,
        id="institution-section"
    )
    # Output layout
    children = [
        intro_section,
        description_section,
        product_section,
        institution_section
    ]
    page_layout = html.Div(
        children=children,
        id="home-body"
    )
    return page_layout


