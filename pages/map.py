

import os
import json
import toml
import numpy as np
import pandas as pd

import dash
from dash import Dash, html, dcc
from dash import callback, Output, Input, State
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc

import methods as mt


#%% Config
PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
layer = mt.load_layers()
layer_data = mt.load_data_layers()

MAP_VALUES_RANGE = {
    "Precip": (600, 1200),
    "PET": (1000, 1600),
    "Et": (300, 700),
    "Infil": (300, 900),
    "Rg": (0, 300)
}


#%% Page Layout
dash.register_page(__name__, path="/App", title="Aplicacion")

def layout(lang="es", **other_strings):
    if lang not in ("es", "en"):
        lang = "es"
    with open(os.path.join(PATH, "static", "labels", f"map_labels_{lang}.toml"), "r", encoding="utf-8") as fid:
        labels = toml.load(fid)
    # Layout - Children
    data_div = dbc.Col(
        html.Div(
            [
                dcc.Markdown(labels["options-col"]["label1"]),
                dbc.Accordion(
                    [
                        dbc.AccordionItem(
                            [
                                dcc.Markdown(labels["options-col"]["label2"], id="dataset-help"),
                                dbc.Popover(
                                    dbc.PopoverBody(dcc.Markdown(labels["options-col"]["text1"])),
                                    target="dataset-help",
                                    trigger="hover",
                                    placement="auto"
                                ),
                                dcc.Dropdown(
                                    labels["datasets-list"],
                                    "m4",
                                    clearable=False,
                                    id="dataset-dropdown"
                                ),
                                dcc.Markdown(labels["options-col"]["label3"]),
                                dcc.Dropdown(
                                    labels["datatype-dropdown"],
                                    "grid",
                                    clearable=False,
                                    id="datatype-dropdown"
                                ),
                            ],
                            title=labels["options-col"]["label4"],
                            id="dataset-options-accordion"
                        ),
                        dbc.AccordionItem(
                            [
                                dcc.Markdown(labels["options-col"]["label5"]),
                                dcc.Dropdown(
                                    labels["map-variable-dropdown"],
                                    "Rg",
                                    clearable=False,
                                    id="map-variable-dropdown"
                                ),
                                dcc.Markdown(labels["options-col"]["label6"]),
                                dcc.Dropdown(
                                    ["Spectral", "YlGnBu", "Viridis"],
                                    "Spectral",
                                    clearable=False,
                                    id="map-color-dropdown"
                                ),
                                dcc.Markdown(labels["options-col"]["label7"]),
                                dcc.Input(id="map-min-input", type="number", min=0, max=3000, step=1, value=0),
                                dcc.Markdown(labels["options-col"]["label8"]),
                                dcc.Input(id="map-max-input", type="number", min=0, max=3000, step=1, value=300),
                            ],
                            title=labels["options-col"]["label9"],
                            id="map-options-accordion"
                        ),
                        dbc.AccordionItem(
                            [
                                dcc.Markdown(labels["options-col"]["label10"]),
                                dcc.Dropdown(
                                    labels["timescale-dropdown"],
                                    "Annual",
                                    clearable=False,
                                    id="timescale-dropdown"
                                ),
                                dcc.Markdown(labels["options-col"]["label13"]),
                                dcc.Dropdown(
                                    ["mm", "m3/s", "ft3/s", "lps"],
                                    "mm",
                                    clearable=False,
                                    id="units-dropdown"
                                ),
                                dcc.Markdown(labels["options-col"]["label11"]),
                                dcc.Dropdown(
                                    labels["variable-dropdown"],
                                    ["Rg"],
                                    multi=True,
                                    id="variable-dropdown"
                                ),
                            ],
                            title=labels["options-col"]["label12"],
                            id="series-options-accordion"
                        )
                    ],
                    start_collapsed=True,
                ),
                html.Br(),
                dbc.Button(
                    labels["options-col"]["button2"],
                    color="primary",
                    class_name="data-button",
                    id="download-map-button"
                ),
                html.Br(),
                dbc.Spinner(dcc.Download(id="download-map")),
                html.Br(),
                dbc.Button(
                    labels["options-col"]["button1"],
                    color="primary",
                    class_name="data-button",
                    id="download-data-button"
                ),
                dcc.Download(id="download"),
            ],
            id="options-col-div"
        ),
        width=3,
        id="options-col"
    )
    map_div = dbc.Col(
        dbc.Tabs(
            [
                dbc.Tab(
                    [
                        dcc.Slider(
                            min=2001,
                            max=2021,
                            step=1,
                            value=2001,
                            marks={x: f"{x}" for x in range(2001, 2022, 5)},
                            tooltip={"placement": "bottom", "always_visible": False},
                            dots=True,
                            included=False,
                            id="map-year-slider"
                        ),
                        dcc.Graph(
                            figure=mt.create_map(
                                layer,
                                layer_data,
                                2001,
                                "m4",
                                "grid",
                                "Rg",
                                "Spectral",
                                0,
                                300
                            ),
                            responsive=True,
                            id="main-map"
                        )
                    ],
                    label=labels["options-col"]["label14"],
                    id="map-tab-1"
                ),
                dbc.Tab(
                    html.Div(
                        [
                            dcc.Markdown(labels["options-col"]["label16"])
                        ],
                        id="timeseries-div",
                    ),
                    label=labels["options-col"]["label15"],
                    id="map-tab-2",
                ),
            ],
            id="map-data-tabs"
        ),
        width=9,
        id="map-data-col"
    )
    # Page Layout
    children = [
        data_div,
        map_div
    ]
    page_layout = dbc.Row(
        children=children,
        id="map-body"
    )
    return page_layout


#%% App Callbacks
"""
@callback(
    Output("map-body", "children"),
    Input("link-menu2", "n_clicks"),
    State("lang-option", "children")
)
def callback_update_map_layout(n_clicks, lang):
    if lang == "Español":
        lang = "en"
    else:
        lang = "es"
    children = update_map_layout(lang)
    return children
"""

@callback(
    [Output("main-map", "figure"),
     Output("main-map", "selectedData")],
    [Input("map-year-slider", "value"),
     Input("dataset-dropdown", "value"),
     Input("datatype-dropdown", "value"),
     Input("map-variable-dropdown", "value"),
     Input("map-color-dropdown", "value"),
     Input("map-min-input", "value"),
     Input("map-max-input", "value")],
    State("memory", "data")
)
def update_map(year, dataset, dtype, varname,
               cmap, vmin, vmax, data):
    data = data or dict(lang="es")
    lang = data.get("lang", "es")
    figure =  mt.create_map(
        layer,
        layer_data,
        year,
        dataset,
        dtype,
        varname,
        cmap,
        vmin,
        vmax,
        lang=lang
    )
    return figure, None


@callback(
    [Output("map-year-slider", "max"),
     Output("map-year-slider", "value"),
     Output("map-year-slider", "marks")],
    [Input("dataset-dropdown", "value")],
)
def update_year_slider_ranges(dataset):
    if dataset == "m4":
        vmax = 2021
    else:
        vmax = 2016
    year = 2001
    marks = {x: f"{x}" for x in range(2001, vmax+1, 5)}
    return vmax, year, marks


@callback(
    [Output("map-min-input", "value"),
     Output("map-max-input", "value")],
    [Input("map-variable-dropdown", "value")]
)
def update_map_variable_ranges(varname):
    return MAP_VALUES_RANGE[varname]


@callback(
        Output("map-tab-2", "children"),
        [Input("dataset-dropdown", "value"),
         Input("datatype-dropdown", "value"),
         Input("variable-dropdown", "value"),
         Input("timescale-dropdown", "value"),
         Input("units-dropdown", "value"),
         Input("main-map", "selectedData")],
        State("memory", "data")
)
def update_plots(dataset, dtype, variables, ptype, units, selected_data, data):
    data = data or dict(lang="es")
    lang = data.get("lang", "es")
    with open(os.path.join(PATH, "static", "labels", f"map_labels_{lang}.toml"), "r", encoding="utf-8") as fid:
        labels = toml.load(fid)

    if len(variables) == 0:
        return dcc.Markdown(labels["options-col"]["label16"])
    ids = None
    if selected_data is not None:
        ids = [str(point["location"]) for point in selected_data["points"]]
        ids.sort()
        area = mt.get_area(layer_data, dtype, ids)
    else:
        area = mt.get_area(layer_data, dtype, None)
    
    series = mt.read_timeseries(dataset, dtype, variables, ptype, ids, area, units, lang=lang)
    if series:
        fig = mt.plot_series(series, units=units, lang=lang)
        if dtype == "grid" and ids is not None:
            title = labels["options-col"]["label17"]
        else:
            title = labels["datatype-dropdown"][dtype]
        container = dbc.Col(
            [
                html.H3(labels["plot-title"][ptype].format(title)),
                dcc.Graph(
                    figure=fig,
                    #responsive=True
                )
            ]
        )
        return container
    else:
        return dcc.Markdown(labels["options-col"]["label16"])


@callback(
    Output("download", "data"),
    [Input("download-data-button", "n_clicks")],
    [State("dataset-dropdown", "value"),
     State("datatype-dropdown", "value"),
     State("variable-dropdown", "value"),
     State("timescale-dropdown", "value"),
     State("units-dropdown", "value"),
     State("main-map", "selectedData"),
     State("memory", "data")],
     prevent_initial_call=True,
)
def download_data(n_clicks, dataset, dtype, variables, ptype, units, selected_data, data):
    data = data or dict(lang="es")
    lang = data.get("lang", "es")
    if len(variables) == 0:
        return None
    else:
        ids = None
        if selected_data is not None:
            ids = [str(point["location"]) for point in selected_data["points"]]
            ids.sort()
            area = mt.get_area(layer_data, dtype, ids)
        else:
            area = mt.get_area(layer_data, dtype, None)
    
    series = mt.read_timeseries(dataset, dtype, variables, ptype, ids, area, units, lang=lang)
    if series:
        labels = []
        joined_data = []
        for key in series.keys():
            labels.extend([(key, col) for col in series[key].columns])
            joined_data.append(series[key])
        
        columns = pd.MultiIndex.from_tuples(labels, names=["Variable", "Location"])

        export_data = pd.concat(joined_data, axis=1)
        export_data.columns = columns
        dataset = mt.get_dataset_name(dataset)
        if lang == "es":
            basename = f"Resultados_{dataset}_{dtype}_{ptype}_{units}.csv"
        else:
            basename = f"Results_{dataset}_{dtype}_{ptype}_{units}.csv"
        
        return dcc.send_data_frame(
            export_data.to_csv,
            filename=basename)
    else:
        return None


@callback(
    Output("download-map", "data"),
    [Input("download-map-button", "n_clicks")],
    [State("map-year-slider", "value"),
     State("dataset-dropdown", "value"),
     State("datatype-dropdown", "value"),
     State("map-variable-dropdown", "value"),
     State("memory", "data")],
     prevent_initial_call=True
)
def download_map(n_clicks, year, dataset, dtype, varname, data):
    data = data or dict(lang="es")
    lang = data.get("lang", "es")
    output_layer = mt.download_layer(
        layer,
        layer_data,
        year,
        dataset,
        dtype,
        varname,
        lang=lang
    )
    dataset = mt.get_dataset_name(dataset)
    if lang == "es":
        output_filename = f"Capa_{dataset}_{dtype}_{year}.geojson"
    else:
        output_filename = f"Layer_{dataset}_{dtype}_{year}.geojson"
    download = {"content": json.dumps(output_layer), "filename": output_filename}
    return download

